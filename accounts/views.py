from django.shortcuts import render, redirect, get_object_or_404
from accounts.forms import LoginForm, SignupForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User

# Create your views here.


def LoginView(request):

    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("home")

    else:
        form = LoginForm()

    context = {"form": form}

    return render(request, "accounts/login.html", context)


def LogoutView(request):
    logout(request)
    return redirect("login")


def SignupView(request):

    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:

                NewUser = User.objects.create_user(
                    username=username,
                    password=password,
                )

                login(request, NewUser)

            else:
                form.add_error("password", "Passwords do not match")

            return redirect("home")
    else:
        form = SignupForm()

    context = {"form": form}

    return render(request, "registration/signup.html", context)
