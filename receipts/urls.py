from django.urls import path
from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    ExpenseCategoriesListView,
    AccountListView,
    ExpenseCategoryCreateView,
    AccountCreateView,
)

urlpatterns = [
    path("", ReceiptListView, name="home"),
    path("create/", ReceiptCreateView, name="create_receipt"),
    path("categories/", ExpenseCategoriesListView, name="category_list"),
    path("accounts/", AccountListView, name="account_list"),
    path("create/", ExpenseCategoryCreateView, name="create_category"),
    path("accounts/create/", AccountCreateView, name="create_account"),
]
