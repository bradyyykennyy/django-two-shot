from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountCreateForm


@login_required(redirect_field_name="login")
def ReceiptListView(request):
    ReceiptObjects = Receipt.objects.filter(purchaser=request.user)

    Context = {"Receipts": ReceiptObjects}

    return render(request, "list.html", Context)


@login_required(redirect_field_name="login")
def ReceiptCreateView(request):

    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    else:
        form = ReceiptForm()

    context = {"form": form}

    return render(request, "create.html", context)


@login_required(redirect_field_name="login")
def ExpenseCategoryCreateView(request):

    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("home")

    else:
        form = ExpenseCategoryForm()

    context = {"form": form}

    return render(request, "categories/create.html", context)


@login_required(redirect_field_name="login")
def AccountCreateView(request):

    if request.method == "POST":
        form = AccountCreateForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("home")

    else:
        form = AccountCreateForm()

    context = {"form": form}

    return render(request, "accounts/create.html", context)


@login_required(redirect_field_name="login")
def ExpenseCategoriesListView(request):
    ReceiptObjects = Receipt.objects.filter(purchaser=request.user)
    ExpenseCategories = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "Receipts": ReceiptObjects,
        "ExpenseCategories": ExpenseCategories,
    }

    return render(request, "categories/list.html", context)


@login_required(redirect_field_name="login")
def AccountListView(request):
    Accounts = Account.objects.filter(owner=request.user)

    context = {
        "Accounts": Accounts,
    }
    return render(request, "accounts/list.html", context)
